# Android MVP Example #

This is an experiment to to show how does it work an MVP pattern applied to an Android project using OkHttp and Glide Library.

### What is this repository for? ###

* MVP Example applied to Android Project
* Version 1.0

### Android SDK Version ###
The project requires a minimum SDK version of 14

### Dependency ###
The project showing an example thow to use featured libraries on MVP pattern:

* [Cardview](http://developer.android.com/reference/android/support/v7/widget/CardView.html)
* [Recyclerview](http://developer.android.com/reference/android/support/v7/widget/RecyclerView.html)
* [OkHttp](http://square.github.io/okhttp/)
* [Glide](https://github.com/bumptech/glide)
* [Gson](https://github.com/google/gson)

### Author ###
Ciro Rizzo - @JackRix