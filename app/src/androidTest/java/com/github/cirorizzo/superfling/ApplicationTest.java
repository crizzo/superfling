package com.github.cirorizzo.superfling;

import android.test.ApplicationTestCase;

import com.github.cirorizzo.superfling.datamodel.DataSuperFling;
import com.github.cirorizzo.superfling.model.DataRepositoryHelper;
import com.github.cirorizzo.superfling.model.SuperFlingDBHelper;
import com.github.cirorizzo.superfling.presenter.DataFetcherImpl;
import com.github.cirorizzo.superfling.view.DataCardsView;

import org.junit.Test;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<SuperFlingApplication> {
    private SuperFlingApplication testApplication;
    public ApplicationTest() {
        super(SuperFlingApplication.class);
    }

    protected void setUp() throws Exception {
        super.setUp();

        createApplication();
        testApplication = getApplication();
    }

    @Test
    public void testSuperFlingDBHelper() {
        SuperFlingDBHelper superFlingDBHelper = testApplication.superFlingDBHelper;
        assertNotNull(superFlingDBHelper);
    }

    @Test
    public void testTableDB() {
       Exception ex = null;

        try {
            (new DataRepositoryHelper()).checkDB();
        } catch (Exception e) {
            ex = e;
        }
        assertEquals(null, ex);
    }

    @Test
    public void testIsRecordExists() {
        DataSuperFling data = new DataSuperFling();

        (new DataRepositoryHelper()).resetDB();

        data.setID(32832974);
        assertEquals(false, (new DataRepositoryHelper()).isRecordExists(data));

        (new DataRepositoryHelper()).refreshDB(data);
        assertEquals(true, (new DataRepositoryHelper()).isRecordExists(data));

        (new DataRepositoryHelper()).resetDB();
    }

    @Test
    public void testNetworkAPI() {
        WeakReference<DataFetcherImpl> dataFetcherImpl = new WeakReference<>(DataFetcherImpl.getInstance());
        dataFetcherImpl.get().create(new DataCardsView() {
            @Override
            public void setDataCardsView(ArrayList<DataSuperFling> dataSuperFlingList) {
                assertNotNull(dataSuperFlingList);
            }

            @Override
            public void getMessageFromPresenter(String msg) {
                assertNotNull(msg);
            }
        });
    }


}