package com.github.cirorizzo.superfling.datamodel;

import android.provider.BaseColumns;

/**
 * Data Contract for Database/Table
 */
public class SuperFlingDataContract {
    public SuperFlingDataContract() {}

    public static abstract class SuperFlingEntry implements BaseColumns {
        public static final String TABLE_NAME = "superfling_data";
        public static final String COL_DATA_ID = "data_id";
        public static final String COL_IMAGE_ID = "image_id";
        public static final String COL_IMAGE_SIZE = "image_size";
        public static final String COL_IMAGE_WIDTH = "image_width";
        public static final String COL_TITLE = "title";
        public static final String COL_USER_ID = "user_id";
        public static final String COL_USER_NAME = "user_name";
    }
}
