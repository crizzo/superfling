package com.github.cirorizzo.superfling.view;

import com.github.cirorizzo.superfling.datamodel.DataSuperFling;

import java.util.ArrayList;

public interface DataCardsView {
    void setDataCardsView(ArrayList<DataSuperFling> dataSuperFlingList);
    void getMessageFromPresenter(String msg);
}
