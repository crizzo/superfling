package com.github.cirorizzo.superfling.view;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.cirorizzo.superfling.R;
import com.github.cirorizzo.superfling.datamodel.DataSuperFling;
import com.github.cirorizzo.superfling.presenter.DataFetcherImpl;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class MainSuperFlingActivity extends AppCompatActivity implements DataCardsView {
    private final String TAG = MainSuperFlingActivity.class.getSimpleName();

    private RecyclerView containerRecyclerView;
    private SuperFlingAdapter superFlingAdapter;

    private WeakReference<DataFetcherImpl> dataFetcherImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_super_fling);


        // Initialization of Objects for RecyclerView and Adapter
        containerRecyclerView = (RecyclerView) findViewById(R.id.data_recycler_view);
        containerRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        superFlingAdapter = new SuperFlingAdapter(this);
        containerRecyclerView.setAdapter(superFlingAdapter);


        // Used a WeakReference for the Presenter to avoid any kind of Memory Leaks
        dataFetcherImpl = new WeakReference<>(DataFetcherImpl.getInstance());
        dataFetcherImpl.get().create(this);

        try {
            if (hasConnectionAvailable()) {
                dataFetcherImpl.get().run();
            } else {
                Toast.makeText(this, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Error on Fetch: " + e.getLocalizedMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.superfling_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if  (item.getItemId() == R.id.action_usage_report) {
            dataFetcherImpl.get().getUsageReport();

            Toast.makeText(this, getString(R.string.check_logcat_statistics), Toast.LENGTH_LONG).show();

            return true;
        }
        return false;
    }

    /**
     * Implementation for Populate Views through Adapter
     *
     * @param dataSuperFlingList
     */
    @Override
    public void setDataCardsView(final ArrayList<DataSuperFling> dataSuperFlingList) {

        runOnUiThread(new Runnable() {
            public void run() {
                superFlingAdapter.setData(dataSuperFlingList);
                superFlingAdapter.notifyDataSetChanged();
            }
      });

    }

    @Override
    public void getMessageFromPresenter(String msg) {

        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }


    public boolean hasConnectionAvailable() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }
}
