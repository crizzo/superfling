package com.github.cirorizzo.superfling.presenter;

import com.github.cirorizzo.superfling.view.DataCardsView;

public interface DataFetcher {
    void create(DataCardsView dataCardsView);
}
