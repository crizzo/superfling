package com.github.cirorizzo.superfling.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.github.cirorizzo.superfling.datamodel.SuperFlingDataContract;

public class SuperFlingDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static String DATABASE_NAME = "SuperFling.db";

    private final String TEXT_TYPE = " TEXT";
    private final String INT_TYPE = " INTEGER";
    private final String COMMA_SEP = ",";


    private final String SQL_CREATE_TABLE =
            "CREATE TABLE " + SuperFlingDataContract.SuperFlingEntry.TABLE_NAME + " (" +
                    SuperFlingDataContract.SuperFlingEntry._ID + " INTEGER PRIMARY KEY," +
                    SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID + INT_TYPE + COMMA_SEP +
                    SuperFlingDataContract.SuperFlingEntry.COL_IMAGE_ID + INT_TYPE + COMMA_SEP +
                    SuperFlingDataContract.SuperFlingEntry.COL_IMAGE_SIZE + INT_TYPE + COMMA_SEP +
                    SuperFlingDataContract.SuperFlingEntry.COL_IMAGE_WIDTH + INT_TYPE + COMMA_SEP +
                    SuperFlingDataContract.SuperFlingEntry.COL_TITLE + TEXT_TYPE + COMMA_SEP +
                    SuperFlingDataContract.SuperFlingEntry.COL_USER_ID + INT_TYPE + COMMA_SEP +
                    SuperFlingDataContract.SuperFlingEntry.COL_USER_NAME + TEXT_TYPE +
            " )";

    public SuperFlingDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SuperFlingDataContract.SuperFlingEntry.TABLE_NAME);
        db.execSQL(SQL_CREATE_TABLE);
    }
}
