package com.github.cirorizzo.superfling.model;

import android.util.Log;

import java.util.concurrent.Callable;

public abstract class Repository {
    private final String TAG = Repository.class.getSimpleName();

    private class Run {
        public void inSession(Runnable runnable) {
            open();
            runnable.run();
        }
    }

    protected void runInSession(Runnable runnable) {
        new Run().inSession(runnable);
    }

    private class Call<V> {
        public V inSession(Callable<V> callable) {

            open();
            V result = null;
            try {
                result = callable.call();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "inSession: " + e.getMessage());
            }

            return result;
        }
    }

    protected <V> V callInSession(Callable<V> callable) {
        return new Call<V>().inSession(callable);
    }

    protected abstract void open();
}
