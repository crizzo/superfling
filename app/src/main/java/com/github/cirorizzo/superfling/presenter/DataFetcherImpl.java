package com.github.cirorizzo.superfling.presenter;

import android.database.Cursor;
import android.util.Log;

import com.github.cirorizzo.superfling.BuildConfig;
import com.github.cirorizzo.superfling.datamodel.DataSuperFling;
import com.github.cirorizzo.superfling.datamodel.SuperFlingDataContract;
import com.github.cirorizzo.superfling.model.DataRepositoryHelper;
import com.github.cirorizzo.superfling.view.DataCardsView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class DataFetcherImpl implements DataFetcher {
    private final String TAG = DataFetcherImpl.class.getSimpleName();
    private static DataFetcherImpl instance;
    private DataCardsView dataCardsView;

    private final OkHttpClient client = new OkHttpClient();

    // Thread-safe Flag
    private AtomicBoolean isExecuting = new AtomicBoolean(false);

    /**
     * This is to fake the Instantiation
     */
    public DataFetcherImpl() {
    }

    /**
     * Used a Singleton to prevent multi Instantiations from View
     *
     * @return
     */
    public static DataFetcherImpl getInstance() {
        if (instance == null) {
            instance = new DataFetcherImpl();
        }

        return instance;
    }

    @Override
    public void create(DataCardsView dataCardsView) {
        this.dataCardsView = dataCardsView;
    }


    /**
     * Entry point for Network process managed by OkHTTP Library
     *
     * @throws Exception
     */
    public void run() throws Exception {
        // A Custom Check with Thread-safe AtomicBoolan to ensure only one thread is executed
        //  This prevent parallel unuseful Network Requests
        if (isExecuting.get() == false) {

            isExecuting.set(true);
            Request request = new Request.Builder()
                    .url(BuildConfig.SUPERFLING_API_DATA)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    dataCardsView.getMessageFromPresenter("Network Error - Failure" + e.getMessage());
                    isExecuting.set(false);
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        dataCardsView.getMessageFromPresenter("Network Error - Unexpected code " + response);
                        throw new IOException("Unexpected code " + response);
                    }

                    Gson gson = new Gson();

                    ArrayList<DataSuperFling> dataSuperFlingList =
                            gson.fromJson(response.body().charStream(), new TypeToken<ArrayList<DataSuperFling>>() {
                            }.getType());

                    fetchedData(dataSuperFlingList);

                    isExecuting.set(false);
                }
            });
        }
    }

    /**
     * Dispatcher for Refreshing Views and Database
     *
     * @param dataSuperFlingList
     */
    private void fetchedData(ArrayList<DataSuperFling> dataSuperFlingList) {
        // Refresh Database
        new DataRepositoryHelper().refreshDBFromList(dataSuperFlingList);

        // Refresh View
        dataCardsView.setDataCardsView(dataSuperFlingList);
    }

    /**
     * Generate on LogCat the Usage Report
     *
     */
    public void getUsageReport() {
        Cursor c = (new DataRepositoryHelper()).getUsageReport();

        Log.i("Usage Report", "--------------------------------------------------------------");
        Log.i("Usage Report", "------              USAGE REPORT STATISTICS             ------");
        Log.i("Usage Report", "--------------------------------------------------------------");
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                Log.i("Usage Report", String.format("User %s - Number of Posts %d",
                        c.getString(c.getColumnIndex(SuperFlingDataContract.SuperFlingEntry.COL_USER_NAME)),
                        c.getInt(c.getColumnIndex("POSTS"))));
                c.moveToNext();
            }
        }
        Log.i("Usage Report", "--------------------------------------------------------------");
    }
}
