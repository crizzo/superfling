package com.github.cirorizzo.superfling.datamodel;

import android.content.ContentValues;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Deserialized Class for JSon Network Result
 * Used as Serialized class for DB activities
 */
public class DataSuperFling {

    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("ImageID")
    @Expose
    private Integer imageID;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("UserName")
    @Expose
    private String userName;

    // Extra Fields for Images
    public int image_size = 0;
    public int image_width = 0;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getImageID() {
        return imageID;
    }

    public void setImageID(Integer imageID) {
        this.imageID = imageID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Methid used as ContentValues for DB Activities
     * @return
     */
    public ContentValues getContentValues() {
        ContentValues contentValues =  new ContentValues();

        contentValues.put(SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID, getID());
        contentValues.put(SuperFlingDataContract.SuperFlingEntry.COL_IMAGE_ID, getImageID());
        contentValues.put(SuperFlingDataContract.SuperFlingEntry.COL_TITLE, getTitle());
        contentValues.put(SuperFlingDataContract.SuperFlingEntry.COL_USER_ID, getUserID());
        contentValues.put(SuperFlingDataContract.SuperFlingEntry.COL_USER_NAME, getUserName());

        contentValues.put(SuperFlingDataContract.SuperFlingEntry.COL_IMAGE_SIZE, image_size);
        contentValues.put(SuperFlingDataContract.SuperFlingEntry.COL_IMAGE_WIDTH, image_width);

        return contentValues;
    }

}
