package com.github.cirorizzo.superfling.view;

import android.view.View;

public interface ImageClickListener {
    void imageClickListener(int position, View v);
}
