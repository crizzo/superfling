package com.github.cirorizzo.superfling.datamodel;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.cirorizzo.superfling.R;
import com.github.cirorizzo.superfling.view.ImageClickListener;

/**
 * ViewHolder Class
 *
 */
public class DataHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final String TAG = DataHolder.class.getSimpleName();

    private ImageClickListener imageClickListener;

    public TextView info_text;
    public ImageView info_image;

    public DataHolder(ImageClickListener imageClickListener, View itemView) {
        super(itemView);

        this.imageClickListener = imageClickListener;
        info_text = (TextView) itemView.findViewById(R.id.info_text);
        info_image = (ImageView) itemView.findViewById(R.id.info_image);

        info_image.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        imageClickListener.imageClickListener(getAdapterPosition(), v);
    }


}
