package com.github.cirorizzo.superfling.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.cirorizzo.superfling.BuildConfig;
import com.github.cirorizzo.superfling.R;
import com.github.cirorizzo.superfling.datamodel.DataHolder;
import com.github.cirorizzo.superfling.datamodel.DataSuperFling;

import java.util.ArrayList;

public class SuperFlingAdapter extends RecyclerView.Adapter<DataHolder> implements ImageClickListener {
    private final String TAG = SuperFlingAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<DataSuperFling> dataSuperFlingArrayList;

    private ValueAnimator flipAnimator;

    public SuperFlingAdapter(Context context) {
        this.context = context;
    }


    /**
     * Method to Update/Refresh Dataset for the Adapter
     *
     * @param dataSuperFlingArrayList
     */
    public void setData(ArrayList<DataSuperFling> dataSuperFlingArrayList) {
        this.dataSuperFlingArrayList = dataSuperFlingArrayList;


        for (DataSuperFling dataSuperFling : this.dataSuperFlingArrayList) {
            Log.d(TAG, String.format("FillUp Adapter with Title: %s", dataSuperFling.getTitle()));
        }
        Log.d(TAG, String.format("Numbers of Data Row: %d", dataSuperFlingArrayList.size()));
    }


    @Override
    public DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cardview, parent, false);

        return new DataHolder(this, view);
    }

    @Override
    public void onBindViewHolder(final DataHolder holder, final int position) {
        holder.info_text.setText(dataSuperFlingArrayList.get(position).getTitle());

        Glide.with(context)
                .load(BuildConfig.SUPERFLING_API_PHOTO + dataSuperFlingArrayList.get(position).getImageID())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.info_image);
    }

    @Override
    public int getItemCount() {
        return (dataSuperFlingArrayList != null) ? dataSuperFlingArrayList.size() : 0;
    }

    /**
     * Custom Click Listener to manage Flipping Animation
     *
     * @param position
     * @param v
     */
    @Override
    public void imageClickListener(int position, View v) {
        final long FULL_ANIMATION_DURATION = 2000;

        flipAnimator = ObjectAnimator.ofFloat((ImageView) v, "rotationX", 0f, 360f);
        flipAnimator.setDuration(FULL_ANIMATION_DURATION);
        flipAnimator.start();
    }

}
