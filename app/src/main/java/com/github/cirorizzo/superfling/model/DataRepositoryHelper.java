package com.github.cirorizzo.superfling.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.github.cirorizzo.superfling.SuperFlingApplication;
import com.github.cirorizzo.superfling.datamodel.DataSuperFling;
import com.github.cirorizzo.superfling.datamodel.SuperFlingDataContract;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class DataRepositoryHelper extends Repository {
    private final String TAG = DataRepositoryHelper.class.getSimpleName();

    private SQLiteDatabase dbReaderWriter;


    public DataRepositoryHelper() {
    }

    /**
     * Entry point for massive Insert/Update Database from a List
     * @param dataSuperFlingArrayList
     */
    public void refreshDBFromList(ArrayList<DataSuperFling> dataSuperFlingArrayList) {
        for (DataSuperFling dataSuperFling : dataSuperFlingArrayList) {
            refreshDB(dataSuperFling);
        }

        Log.d(TAG, String.format("Number of Total Rows: %d", checkDB()));
    }


    /**
     * Entry point for Insert/Update Database
     * @param data
     */
    public void refreshDB(DataSuperFling data) {
        if (isRecordExists(data)) {
            updateDB(data);
        } else {
            insertDB(data);
        }
    }

    /**
     * Check if Record with ID exists
     * @param data
     * @return
     */
    public boolean isRecordExists(final DataSuperFling data) {
        return callInSession(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                boolean result = false;

                Cursor cursor = dbReaderWriter.query(
                        SuperFlingDataContract.SuperFlingEntry.TABLE_NAME,
                        new String[] {SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID},
                        SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID + " = ?",
                        new String[] {String.valueOf(data.getID())},
                        null,
                        null,
                        null);

                if (cursor.moveToFirst()) {
                    if (cursor.getCount() == 0) {
                        result = false;
                    } else {
                        result = true;
                    }
                }

                cursor.close();

                return result;
            }
        });
    }

    /**
     * Insert a Row in DB
     * @param data
     */
    public void insertDB(final DataSuperFling data) {
        runInSession(new Runnable() {
            @Override
            public void run() {
                dbReaderWriter.insert(
                        SuperFlingDataContract.SuperFlingEntry.TABLE_NAME,
                        null,
                        data.getContentValues());
            }
        });
    }

    /**
     * Update a Row in DB
     * @param data
     */
    public void updateDB(final DataSuperFling data) {
        runInSession(new Runnable() {
            @Override
            public void run() {
                dbReaderWriter.update(
                        SuperFlingDataContract.SuperFlingEntry.TABLE_NAME,
                        data.getContentValues(),
                        SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID + " = ?",
                        new String[]{String.valueOf(data.getID())});
            }
        });
    }

    public Cursor getUsageReport() {
        return callInSession(new Callable<Cursor>() {
            @Override
            public Cursor call() throws Exception {
                Cursor c = dbReaderWriter.query(
                        SuperFlingDataContract.SuperFlingEntry.TABLE_NAME,
                        new String[] {SuperFlingDataContract.SuperFlingEntry.COL_USER_NAME, "COUNT(*) AS POSTS"},
                        null,
                        null,
                        SuperFlingDataContract.SuperFlingEntry.COL_USER_ID,
                        null,
                        null
                );
                return c;
            }
        });
    }

    /**
     * Used For Testing
     */
    public int checkDB() {
        return callInSession(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                Cursor c = dbReaderWriter.query(
                        SuperFlingDataContract.SuperFlingEntry.TABLE_NAME,
                        new String[] {SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID},
                        null,
                        null,
                        null,
                        null,
                        null
                );

                int result = 0;

                if (c.moveToFirst()) {
                    result = c.getCount();
                }

                c.close();

                return result;
            }
        });
    }

    /**
     * Used For Testing
     */
    public void resetDB() {
        runInSession(new Runnable() {
            @Override
            public void run() {
                dbReaderWriter.delete(
                        SuperFlingDataContract.SuperFlingEntry.TABLE_NAME,
                        null,
                        null
                );
            }
        });
    }

    /**
     * Used For Testing
     */
    public Cursor getRow(final int iD) {
        return callInSession(new Callable<Cursor>() {
            @Override
            public Cursor call() throws Exception {
                return dbReaderWriter.query(
                        SuperFlingDataContract.SuperFlingEntry.TABLE_NAME,
                        new String[] {SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID},
                        SuperFlingDataContract.SuperFlingEntry.COL_DATA_ID + " = ?",
                        new String[] {String.valueOf(iD)},
                        null,
                        null,
                        null);
            }
        });
    }

    @Override
    protected void open() {
        dbReaderWriter = SuperFlingApplication.superFlingDBHelper.getWritableDatabase();
    }
}
