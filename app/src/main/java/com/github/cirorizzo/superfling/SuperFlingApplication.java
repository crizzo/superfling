package com.github.cirorizzo.superfling;

import android.app.Application;

import com.github.cirorizzo.superfling.model.SuperFlingDBHelper;

public class SuperFlingApplication extends Application {
    public static SuperFlingDBHelper superFlingDBHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        superFlingDBHelper = new SuperFlingDBHelper(this);
    }
}
